import axios from "axios";
import { authConfig, baseUrl, getLogger, withLogs } from "../../core";
import { SneakerProps } from "./SneakerProps";

const sneakerUrl = `http://${baseUrl}/api/sneaker`;

export const getSneakers: (token: string) => Promise<SneakerProps[]> = (
  token
) => {
  return withLogs(axios.get(sneakerUrl, authConfig(token)), "getSneakers");
};

export const getSneakersPage: (
  token: string,
  page: number,
  items: number,
  brandFilter: string | undefined
) => Promise<SneakerProps[]> = (token, page, items, brandFilter) => {
  return withLogs(
    axios.get(
      `${sneakerUrl}/${items}/${page}${brandFilter ? `?brand=${brandFilter}`: ""}`,
      authConfig(token)
    ),
    "getSneakersPage"
  );
};

export const getBrands: (token: string) => Promise<SneakerProps[]> = (
  token
) => {
  return withLogs(
    axios.get(`${sneakerUrl}/brands`, authConfig(token)),
    "getBrands"
  );
};

export const createSneaker: (
  token: string,
  sneaker: SneakerProps
) => Promise<SneakerProps[]> = (token, sneaker) => {
  return withLogs(
    axios.post(sneakerUrl, sneaker, authConfig(token)),
    "createSneaker"
  );
};

export const updateSneaker: (
  token: string,
  sneaker: SneakerProps
) => Promise<SneakerProps[]> = (token, sneaker) => {
  return withLogs(
    axios.put(`${sneakerUrl}/${sneaker._id}`, sneaker, authConfig(token)),
    "updateSneaker"
  );
};

interface MessageData {
  type: string;
  payload: SneakerProps;
}

const log = getLogger("ws");

export const newWebSocket = (
  token: string,
  onMessage: (data: MessageData) => void
) => {
  const ws = new WebSocket(`ws://${baseUrl}`);
  ws.onopen = () => {
    log("web socket onopen");
    ws.send(JSON.stringify({ type: "authorization", payload: { token } }));
  };
  ws.onclose = () => {
    log("web socket onclose");
  };
  ws.onerror = (error) => {
    log("web socket onerror", error);
  };
  ws.onmessage = (messageEvent) => {
    log("web socket onmessage");
    onMessage(JSON.parse(messageEvent.data));
  };
  return () => {
    ws.close();
  };
};
