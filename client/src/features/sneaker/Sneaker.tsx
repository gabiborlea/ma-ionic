import React from "react";
import { IonButton, IonImg, IonItem, IonLabel } from "@ionic/react";
import { SneakerProps } from "./SneakerProps";
import { MyMap } from "../map/MyMap";
import { MyModal } from "../MyModal";

interface SneakerPropsExtended extends SneakerProps {
  onEdit: (_id?: string) => void;
}

const Sneaker: React.FC<SneakerPropsExtended> = (
  props: SneakerPropsExtended
) => {
  return (
    <IonItem onClick={() => props.onEdit(props._id)}>
      <IonLabel>{props.brand}</IonLabel>
      <IonLabel>{props.model}</IonLabel>
      <IonLabel>{props.price + " RON"}</IonLabel>
      <IonLabel>{props.photo && <IonImg src={props.photo} />}</IonLabel>
      {props.lat && props.lng && (
        <MyModal title="Map">
          <MyMap lat={props.lat} lng={props.lng} />
        </MyModal>
      )}
    </IonItem>
  );
};

export default Sneaker;
