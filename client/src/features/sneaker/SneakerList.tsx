import React, { useContext } from "react";
import { RouteComponentProps } from "react-router";
import {
  createAnimation,
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonItem,
  IonList,
  IonLoading,
  IonPage,
  IonSearchbar,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { add } from "ionicons/icons";
import Sneaker from "./Sneaker";
import { SneakerProps } from "./SneakerProps";
import { getLogger } from "../../core";
import { SneakerContext } from "./SneakerProvider";
import { useAppState } from "../hooks/useAppState";
import { useNetwork } from "../hooks/useNetwork";
import { AuthContext } from "../auth";

const log = getLogger("SneakerList");

const SneakerList: React.FC<RouteComponentProps> = ({ history }) => {
  const {
    sneakers,
    fetching,
    fetchingError,
    increasePage,
    disableInfiniteScroll,
    brands,
    setBrandFilter,
    brandFilter,
    modelSearch,
    setModelSearch,
  } = useContext(SneakerContext);
  const { appState } = useAppState();
  const { networkStatus } = useNetwork();
  const { logout } = useContext(AuthContext);

  async function searchNext($event: CustomEvent<void>) {
    increasePage && increasePage();

    ($event.target as HTMLIonInfiniteScrollElement).complete();
  }

  const handleLogout = () => {
    log("logout");
    logout?.();
  };

  React.useEffect(() => {
    const el = document.querySelector(".title");
    if (el) {
      const animation = createAnimation()
        .addElement(el)
        .duration(1000)
        .direction("alternate")
        .iterations(Infinity)
        .keyframes([
          { offset: 0, transform: "scale(3)", opacity: "1" },
          {
            offset: 1,
            transform: "scale(1.5)",
            opacity: "0.5",
          },
        ]);
      animation.play();
    }
  }, []);

  log("render");
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Sneaker List</IonTitle>
          <IonButtons slot="end">
            <div>App state: {appState.isActive ? "active" : "not active"}|</div>
            <div>{networkStatus.connected ? "online" : "offline"}</div>
            <IonButton onClick={handleLogout}>Logout</IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonSelect
          value={brandFilter}
          placeholder="Select Brand"
          onIonChange={(e) => setBrandFilter && setBrandFilter(e.detail.value)}
        >
          {brands?.map((brand) => (
            <IonSelectOption key={brand} value={brand}>
              {brand}
            </IonSelectOption>
          ))}
        </IonSelect>
        <IonSearchbar
          value={modelSearch}
          debounce={1000}
          onIonChange={(e) => setModelSearch && setModelSearch(e.detail.value!)}
        ></IonSearchbar>
        <IonLoading isOpen={fetching} message="Fetching sneakers" />
        <IonItem>
          <div
            style={{
              display: "flex",
              width: "100%",
              height: "50px",
              justifyContent: "center",
              alignItems: "center",
              fontSize: "10px",
            }}
          >
            <div className="title">Sneaker Market</div>
          </div>
        </IonItem>
        {sneakers && (
          <IonList>
            {sneakers
              .filter(
                modelSearch && modelSearch !== ""
                  ? (sneaker) => sneaker.model.indexOf(modelSearch) >= 0
                  : (sneaker) => sneaker
              )
              .map((props: SneakerProps) => (
                <Sneaker
                  key={props._id}
                  brand={props.brand}
                  model={props.model}
                  color={props.color}
                  size={props.size}
                  price={props.price}
                  photo={props.photo}
                  lat={props.lat}
                  lng={props.lng}
                  onEdit={() => history.push(`/sneaker/${props._id}`)}
                />
              ))}
          </IonList>
        )}
        <IonInfiniteScroll
          threshold="30px"
          disabled={disableInfiniteScroll}
          onIonInfinite={(e: CustomEvent<void>) => searchNext(e)}
        >
          <IonInfiniteScrollContent loadingText="Loading more "></IonInfiniteScrollContent>
        </IonInfiniteScroll>
        {fetchingError && (
          <div>{fetchingError.message || "Failed to fetch sneakers"}</div>
        )}
        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton onClick={() => history.push("/sneaker")}>
            <IonIcon icon={add} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default SneakerList;
