import React, { useCallback, useContext, useEffect, useReducer } from "react";
import PropTypes from "prop-types";
import { getLogger } from "../../core";
import { SneakerProps } from "./SneakerProps";
import {
  createSneaker,
  getBrands,
  getSneakers,
  getSneakersPage,
  newWebSocket,
  updateSneaker,
} from "./sneakerApi";
import { AuthContext } from "../auth";
import { Storage } from "@capacitor/storage";
import { useNetwork } from "../hooks/useNetwork";

const log = getLogger("SneakerProvider");

type SaveSneakerFunction = (Sneaker: SneakerProps) => Promise<any>;

export interface SneakersState {
  sneakers?: SneakerProps[];
  brands?: string[];
  fetching: boolean;
  fetchingError?: Error | null;
  saving: boolean;
  savingError?: Error | null;
  saveSneaker?: SaveSneakerFunction;
  page?: number;
  items?: number;
  increasePage?: Function;
  disableInfiniteScroll?: boolean;
  brandFilter?: string;
  setBrandFilter?: Function;
  modelSearch?: string;
  setModelSearch?: Function;
}

interface ActionProps {
  type: string;
  payload?: any;
}

const initialState: SneakersState = {
  fetching: false,
  saving: false,
  page: 1,
  items: 15,
};

const FETCH_SNEAKERS_STARTED = "FETCH_SNEAKERS_STARTED";
const FETCH_SNEAKERS_SUCCEEDED = "FETCH_SNEAKERS_SUCCEEDED";
const FETCH_SNEAKERS_FAILED = "FETCH_SNEAKERS_FAILED";
const SAVE_SNEAKER_STARTED = "SAVE_SNEAKER_STARTED";
const SAVE_SNEAKER_SUCCEEDED = "SAVE_SNEAKER_SUCCEEDED";
const SAVE_SNEAKER_FAILED = "SAVE_SNEAKER_FAILED";
const INCREASE_PAGE = "INCREASE_PAGE";
const SET_INFINITE_SCROLL = "SET_INFINITE_SCROLL ";
const FETCH_BRANDS_SUCCEEDED = "FETCH_BRANDS_SUCCEEDED";
const SET_BRAND_FILTER = "SET_BRAND_FILTER";
const SET_MODEL_SEARCH = "SET_MODEL_SEARCH";

const reducer: (state: SneakersState, action: ActionProps) => SneakersState = (
  state,
  { type, payload }
) => {
  switch (type) {
    case FETCH_SNEAKERS_STARTED:
      return { ...state, fetching: true, fetchingError: null };
    case FETCH_SNEAKERS_SUCCEEDED:
      if (payload.pagination) {
        return {
          ...state,
          sneakers: state.sneakers
            ? [...state.sneakers, ...payload.sneakers]
            : payload.sneakers,
          fetching: false,
        };
      }
      return { ...state, sneakers: payload.sneakers, fetching: false };
    case FETCH_SNEAKERS_FAILED:
      return { ...state, fetchingError: payload.error, fetching: false };
    case SAVE_SNEAKER_STARTED:
      return { ...state, savingError: null, saving: true };
    case SAVE_SNEAKER_SUCCEEDED:
      const sneakers = [...(state.sneakers || [])];
      const sneaker = payload.sneaker;
      const index = sneakers.findIndex((it) => it._id === sneaker._id);
      if (index === -1) {
        sneakers.splice(0, 0, sneaker);
      } else {
        sneakers[index] = sneaker;
      }
      return { ...state, sneakers, saving: false };
    case SAVE_SNEAKER_FAILED:
      return { ...state, savingError: payload.error, saving: false };
    case INCREASE_PAGE:
      return { ...state, page: state.page ? state.page + 1 : undefined };
    case SET_INFINITE_SCROLL:
      return { ...state, disableInfiniteScroll: payload.disable };
    case FETCH_BRANDS_SUCCEEDED:
      return { ...state, brands: payload.brands };
    case SET_BRAND_FILTER:
      return { ...state, sneakers: [], page: 1, brandFilter: payload.brand };
    case SET_MODEL_SEARCH:
      return { ...state, modelSearch: payload.model };
    default:
      return state;
  }
};

export const SneakerContext = React.createContext<SneakersState>(initialState);

interface SneakerProviderProps {
  children: PropTypes.ReactNodeLike;
}

export const SneakerProvider: React.FC<SneakerProviderProps> = ({
  children,
}) => {
  const { networkStatus } = useNetwork();
  const { token } = useContext(AuthContext);
  const [state, dispatch] = useReducer(reducer, initialState);
  const {
    sneakers,
    fetching,
    fetchingError,
    saving,
    savingError,
    disableInfiniteScroll,
    page,
    items,
    brands,
    brandFilter,
    modelSearch,
  } = state;
  useEffect(getSneakersEffect, [token, page, items, brandFilter]);
  useEffect(getBrandsEffect, [token, sneakers]);
  useEffect(wsEffect, [token]);
  const saveSneaker = useCallback<SaveSneakerFunction>(saveSneakerCallback, [
    networkStatus.connected,
    token,
  ]);
  useEffect(saveSneakersFromLocalEffect, [networkStatus, saveSneaker]);
  const value = {
    sneakers,
    fetching,
    fetchingError,
    saving,
    savingError,
    saveSneaker,
    increasePage,
    disableInfiniteScroll,
    brands,
    setBrandFilter,
    brandFilter,
    modelSearch,
    setModelSearch,
  };
  log("returns");
  return (
    <SneakerContext.Provider value={value}>{children}</SneakerContext.Provider>
  );

  function increasePage() {
    dispatch({ type: INCREASE_PAGE });
  }

  function setBrandFilter(brand: string) {
    dispatch({ type: SET_BRAND_FILTER, payload: { brand } });
  }

  function setModelSearch(model: string) {
    dispatch({ type: SET_MODEL_SEARCH, payload: { model } });
  }

  function saveSneakersFromLocalEffect() {
    console.log("HERE");
    saveSneakers();
    async function saveSneakers() {
      const res = await Storage.get({ key: "sneakers" });
      if (res.value) {
        const sneakers = JSON.parse(res.value);
        for (const sneaker of sneakers) {
          saveSneaker(sneaker);
        }
        Storage.remove({ key: "sneakers" });
      }
    }
  }

  function getSneakersEffect() {
    let canceled = false;
    fetchSneakers();
    return () => {
      canceled = true;
    };

    async function fetchSneakers() {
      if (!token?.trim()) {
        return;
      }
      try {
        log("fetchSneakers started");
        dispatch({ type: FETCH_SNEAKERS_STARTED });
        let sneakers;
        if (page && items) {
          sneakers = await getSneakersPage(token, page, items, brandFilter);
          log("fetchSneakers succeeded");
          if (!canceled) {
            dispatch({
              type: FETCH_SNEAKERS_SUCCEEDED,
              payload: { sneakers, pagination: true },
            });

            if (sneakers.length < items) {
              dispatch({
                type: SET_INFINITE_SCROLL,
                payload: { disable: true },
              });
            }
          }
        } else {
          sneakers = await getSneakers(token);
          log("fetchSneakers succeeded");
          if (!canceled) {
            dispatch({ type: FETCH_SNEAKERS_SUCCEEDED, payload: { sneakers } });
          }
        }
      } catch (error) {
        log("fetchSneakers failed");
        dispatch({ type: FETCH_SNEAKERS_FAILED, payload: { error } });
      }
    }
  }

  function getBrandsEffect() {
    let canceled = false;
    fetchBrands();
    return () => {
      canceled = true;
    };

    async function fetchBrands() {
      if (!token?.trim()) {
        return;
      }
      try {
        log("fetchBrands started");
        const brands = await getBrands(token);
        log("fetchBrands succeeded");
        if (!canceled) {
          dispatch({ type: FETCH_BRANDS_SUCCEEDED, payload: { brands } });
        }
      } catch (error) {
        log("fetchBrands failed");
      }
    }
  }

  async function saveSneakerCallback(sneaker: SneakerProps) {
    if (!networkStatus.connected) {
      saveSneakerLocal(sneaker);
    }
    try {
      log("saveSneaker started");
      dispatch({ type: SAVE_SNEAKER_STARTED });
      const savedSneaker = await (sneaker._id
        ? updateSneaker(token, sneaker)
        : createSneaker(token, sneaker));
      log("saveSneaker succeeded");
      dispatch({
        type: SAVE_SNEAKER_SUCCEEDED,
        payload: { sneaker: savedSneaker },
      });
    } catch (error) {
      log("saveSneaker failed");
      dispatch({ type: SAVE_SNEAKER_FAILED, payload: { error } });
    }
  }

  async function saveSneakerLocal(sneaker: SneakerProps) {
    const res = await Storage.get({ key: "sneakers" });
    if (res.value) {
      const sneakers = JSON.parse(res.value);
      Storage.set({
        key: "sneakers",
        value: JSON.stringify([...sneakers, sneaker]),
      });
    } else {
      Storage.set({ key: "sneakers", value: JSON.stringify([sneaker]) });
    }
  }

  function wsEffect() {
    let canceled = false;
    log("wsEffect - connecting");
    let closeWebSocket: () => void;
    if (token?.trim()) {
      closeWebSocket = newWebSocket(token, (message) => {
        if (canceled) {
          return;
        }
        const { type, payload: sneaker } = message;
        log(`ws message, sneaker ${type}`);
        if (type === "created" || type === "updated") {
          dispatch({ type: SAVE_SNEAKER_SUCCEEDED, payload: { sneaker } });
        }
      });
    }
    return () => {
      log("wsEffect - disconnecting");
      canceled = true;
      closeWebSocket?.();
    };
  }
};
