import React, { useContext, useEffect, useState } from "react";
import {
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonImg,
  IonInput,
  IonItemDivider,
  IonLoading,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { getLogger } from "../../core";
import { SneakerContext } from "./SneakerProvider";
import { RouteComponentProps } from "react-router";
import { SneakerProps } from "./SneakerProps";
import { usePhotoGallery } from "../hooks/usePhotoGallery";
import { camera } from "ionicons/icons";
import { MyMap } from "../map/MyMap";

const log = getLogger("SneakerEdit");

interface SneakerEditProps
  extends RouteComponentProps<{
    id?: string;
  }> {}

const SneakerEdit: React.FC<SneakerEditProps> = ({ history, match }) => {
  const { sneakers, saving, savingError, saveSneaker } =
    useContext(SneakerContext);
  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [color, setColor] = useState("");
  const [size, setSize] = useState(0);
  const [price, setPrice] = useState(0);
  const [sneaker, setSneaker] = useState<SneakerProps>();
  const [photo, setPhoto] = useState("");
  const [lat, setLat] = React.useState(46.7524289);
  const [lng, setLng] = React.useState(23.5872008);
  const { takePhoto } = usePhotoGallery();
  useEffect(() => {
    log("useEffect");
    const routeId = match.params.id || "";
    const sneaker = sneakers?.find((it) => it._id === routeId);
    setSneaker(sneaker);
    if (sneaker) {
      setBrand(sneaker.brand);
      setModel(sneaker.model);
      setColor(sneaker.color);
      setSize(sneaker.size);
      setPrice(sneaker.price);
      if (sneaker.photo){
        setPhoto(sneaker.photo)
      }
      if(sneaker.lng && sneaker.lat) {
        setLat(sneaker.lat);
        setLng(sneaker.lng);
      }
    }
  }, [match.params.id, sneakers]);
  const handleSave = () => {
    const editedSneaker = sneaker
      ? { ...sneaker, brand, model, color, size, price, photo, lat, lng }
      : { brand, model, color, size, price, photo, lat, lng };
    saveSneaker && saveSneaker(editedSneaker).then(() => history.goBack());
  };
  const [openMap, setOpenMap] = React.useState(false);
  log("render");
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Edit</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={handleSave}>Save</IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonItemDivider>{"Brand"}</IonItemDivider>
        <IonInput
          value={brand}
          onIonChange={(e) => setBrand(e.detail.value || "")}
        />
        <IonItemDivider>{"Model"}</IonItemDivider>
        <IonInput
          value={model}
          onIonChange={(e) => setModel(e.detail.value || "")}
        />
        <IonItemDivider>{"Color"}</IonItemDivider>
        <IonInput
          value={color}
          onIonChange={(e) => setColor(e.detail.value || "")}
        />
        <IonItemDivider>{"Size"}</IonItemDivider>
        <IonInput
          type="number"
          value={size}
          onIonChange={(e) => setSize(parseInt(e.detail.value!, 0))}
        />
        <IonItemDivider>{"Price"}</IonItemDivider>
        <IonInput
          type="number"
          value={price}
          style={{ paddingLeft: "10px" }}
          onIonChange={(e) => setPrice(parseInt(e.detail.value!, 0))}
        />
        {photo && <IonImg src={photo} />}
        <IonButton onClick={() => setOpenMap(!openMap)}>Toggle Map</IonButton>
        {openMap &&
          <MyMap
            lat={lat}
            lng={lng}
            onMapClick={(e: any) => {

              setLat(e.latLng.lat()); 
              setLng(e.latLng.lng());
            }}
          />}
        <IonFab vertical="bottom" horizontal="center" slot="fixed">
          <IonFabButton
            onClick={async () => {
              try {
                const image = await takePhoto();
                setPhoto(image);
              } catch (e) {}
            }}
          >
            <IonIcon icon={camera} />
          </IonFabButton>
        </IonFab>
        <IonLoading isOpen={saving} />
        {savingError && (
          <div>{savingError.message || "Failed to save sneaker"}</div>
        )}
      </IonContent>
    </IonPage>
  );
};

export default SneakerEdit;
