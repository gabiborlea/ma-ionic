export interface SneakerProps {
  _id?: string;
  brand: string;
  model: string;
  color: string;
  size: number;
  price: number;
  photo?: string;
  lng?:number;
  lat?:number;
}
