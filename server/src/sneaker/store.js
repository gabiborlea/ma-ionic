import dataStore from 'nedb-promise';

export class SneakerStore {
  constructor({ filename, autoload }) {
    this.store = dataStore({ filename, autoload });
  }
  
  async find(props) {
    return this.store.find(props);
  }
  
  async findOne(props) {
    return this.store.findOne(props);
  }
  
  async insert(sneaker) {
    const {brand, model, color, size, price} = sneaker;
    if (!brand) {
      throw new Error('Missing brand property');
    }
    if (!model) {
      throw new Error('Missing model property');
    }
    if (!color) {
      throw new Error('Missing color property');
    }
    if (!size) {
      throw new Error('Missing size property');
    }
    if (!price) {
      throw new Error('Missing price property');
    }
    return this.store.insert(sneaker);
  };
  
  async update(props, sneaker) {
    return this.store.update(props, sneaker);
  }
  
  async remove(props) {
    return this.store.remove(props);
  }
}

export default new SneakerStore({ filename: './db/sneakers.json', autoload: true });