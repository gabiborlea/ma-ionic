import Router from "koa-router";
import sneakerStore from "./store";
import { broadcast } from "../utils";

export const router = new Router();

router.get("/", async (ctx) => {
  const response = ctx.response;
  const userId = ctx.state.user._id;
  response.body = await sneakerStore.find({ userId });
  response.status = 200; // ok
});

router.get("/:items/:page", async (ctx) => {
  const response = ctx.response;
  const userId = ctx.state.user._id;
  const brand = ctx.query.brand;
  const allSneakers = await sneakerStore.find(
    brand ? { userId, brand } : { userId }
  );
  const page = ctx.params.page;
  const items = ctx.params.items;
  const start = (page - 1) * items;
  const stop =
    page * items <= allSneakers.length ? page * items : allSneakers.length;
  response.body = allSneakers.slice(start, stop);
  response.status = 200; // ok
});

router.get("/brands", async (ctx) => {
  const response = ctx.response;
  const userId = ctx.state.user._id;
  const allSneakers = await sneakerStore.find({ userId });
  const brands = allSneakers
    .map((sneaker) => sneaker.brand)
    .filter((v, i, a) => a.indexOf(v) === i);
  response.body = brands;
  response.status = 200; // ok
});

router.get("/:id", async (ctx) => {
  const userId = ctx.state.user._id;
  const sneaker = await sneakerStore.findOne({ _id: ctx.params.id });
  const response = ctx.response;
  if (sneaker) {
    if (sneaker.userId === userId) {
      response.body = sneaker;
      response.status = 200; // ok
    } else {
      response.status = 403; // forbidden
    }
  } else {
    response.status = 404; // not found
  }
});

const createSneaker = async (ctx, sneaker, response) => {
  try {
    const userId = ctx.state.user._id;
    sneaker.userId = userId;
    const storedSneaker = await sneakerStore.insert(sneaker);
    response.body = storedSneaker;
    response.status = 201; // created
    broadcast(userId, { type: "created", payload: storedSneaker });
  } catch (err) {
    console.error(err);
    response.body = { message: err.message };
    response.status = 400; // bad request
  }
};

router.post(
  "/",
  async (ctx) => await createSneaker(ctx, ctx.request.body, ctx.response)
);

router.put("/:id", async (ctx) => {
  const sneaker = ctx.request.body;
  const id = ctx.params.id;
  const sneakerId = sneaker._id;
  const response = ctx.response;
  if (sneakerId && sneakerId !== id) {
    response.body = { message: "Param id and body _id should be the same" };
    response.status = 400; // bad request
    return;
  }
  if (!sneakerId) {
    await createSneaker(ctx, sneaker, response);
  } else {
    const updatedCount = await sneakerStore.update({ _id: id }, sneaker);
    if (updatedCount === 1) {
      response.body = sneaker;
      response.status = 200; // ok
      const userId = ctx.state.user._id;
      broadcast(userId, { type: "updated", payload: sneaker });
    } else {
      response.body = { message: "Resource no longer exists" };
      response.status = 405; // method not allowed
    }
  }
});

router.del("/:id", async (ctx) => {
  const userId = ctx.state.user._id;
  const sneaker = await sneakerStore.findOne({ _id: ctx.params.id });
  if (sneaker && userId !== sneaker.userId) {
    ctx.response.status = 403; // forbidden
  } else {
    await sneakerStore.remove({ _id: ctx.params.id });
    ctx.response.status = 204; // no content
  }
});
